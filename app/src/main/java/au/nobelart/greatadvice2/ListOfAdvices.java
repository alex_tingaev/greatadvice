package au.nobelart.greatadvice2;

import java.util.ArrayList;

/**
 * artes on 3/08/16. tingaev@gmail.com
 **/
class ListOfAdvices {
    private static ArrayList<Advices> list = new ArrayList<Advices>();
    public ListOfAdvices() {
    }
    public static ArrayList<Advices> getList() {
        return list;
    }
    public static void setList(ArrayList<Advices> list) {
        ListOfAdvices.list = list;
    }
    public void printRandomSavedAdvice(ArrayList list) {

        int i = (int) (Math.random()*list.size()-1);
        System.out.println(list.get(i));
    }
}
