package au.nobelart.greatadvice2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * artes on 4/08/16. tingaev@gmail.com
 **/
class TalkBox implements Runnable {
    private Professions professions;
    private Advices advices;
    private boolean enough = false;
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    TalkBox(Professions professions, Advices advices) {
        this.professions = professions;
        this.advices = advices;
    }

    @Override
    public void run() {
        try {
            say("Эта программа даст вам замечательный совет!");
            Thread.sleep(1000);

            while (!enough) {
                say("Пожалуйста, введите вашу профессию из списка ниже: \n");
                pause(1000);
                professions.printAllProf();
                pause(1000);
                System.out.print("\nВаша профессия: ");

                String profession = reader.readLine();
                profession = formatString(profession);
                pause(500);
                say("Отлично! Ваш совет... ");
                pause(300);
                advices.getNewAdviceFromSite(profession);
                advices.printLastAdvice();
                ListOfAdvices.getList().add(advices);

                pause(1000);
                System.out.print("Хотите еще совет? (да/нет): ");

                if (reader.readLine().equalsIgnoreCase("да")) {
                    pause(500);
                    say("Замечательно! Обожаю давать советы!");
                    pause(500);
                    say("Совет по той же профессии? (да/нет)");
                    if (reader.readLine().equalsIgnoreCase("да"))
                    {
                        while (true) {
                            say("Отлично! Ваш совет... ");
                            pause(500);
                            // !!!
                            advices.getNewAdviceFromSite(profession);
                            advices.printLastAdvice();
                            ListOfAdvices.getList().add(advices);
                            pause(1000);
                            System.out.print("Хотите еще совет? (да/нет): ");
                            if (reader.readLine().equalsIgnoreCase("нет")) {enough= true; break;}
                        }
                    }
                } else {
                    say("Очень жаль. Приходите еще!");
                    pause(500);
                    enough = true;
                }
            }
            reader.close();
        }
        catch (InterruptedException e) {
            say("Какая-то ошибка с нитями, хз что делать(");
        }
        catch (IOException e) {
            say("Какая-то ошибка с вводом, хз что делать(");
        }
    }

    private void pause(int millis) throws InterruptedException {
        Thread.sleep(millis);
    }
    private void say(String line) {
        System.out.println(line);
    }

    private String formatString(String s) {

        StringBuilder sb = new StringBuilder();
        sb.append(s.substring(0, 1).toUpperCase());
        sb.append(s.substring(1).toLowerCase());

        return sb.toString().trim();
    }
}

