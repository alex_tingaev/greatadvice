package au.nobelart.greatadvice2;

import java.io.IOException;
import java.util.ArrayList;

/**
 * artes on 3/08/16. tingaev@gmail.com
 **/
class Advices {
    private Advices theAdvices;
    private int id;
    private String text;
    private String sound;
    private String professions;


    private ArrayList<Advices> list = new ArrayList<Advices>();

    Advices() {
    }

    private Advices(int id, String text, String sound, String professions) {
        this.id = id;
        this.text = text;
        this.sound = sound;
        this.professions = professions;
    }

    public Advices(int id, String text) {
        this.id = id;
        this.text = text;
        sound = null;
        professions = null;
    }


    private String getText() {
        return text;
    }


    public void getNewAdviceFromSite(String professions) throws IOException {

        JSONParser jsonParser = new JSONParser(professions);
        Advices advices = jsonParser.getAdvice();
    }

    public void printLastAdvice() {
        System.out.println();
        System.out.println(theAdvices.getText());
        System.out.println();
    }
}
