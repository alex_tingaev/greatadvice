package au.nobelart.greatadvice2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner mySpinner = (Spinner) findViewById(R.id.spinner);
        TextView textViewForAdvice = (TextView) findViewById(R.id.textViewForAdvice);
        Button buttonToGetAdvice = (Button) findViewById(R.id.button);

        Professions professions = new Professions();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, (List<String>) professions.getMap().keySet());

    }
}
