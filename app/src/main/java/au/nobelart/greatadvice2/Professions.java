package au.nobelart.greatadvice2;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

/**
 * artes on 4/08/16. tingaev@gmail.com
 **/
class Professions {
    private static final TreeMap<String, URL> mapOfProfessions;
    static {
        mapOfProfessions = new TreeMap<String, URL>();
        String staticURL = "http://fucking-great-advice.ru/api/random_by_tag/";

        try {
        mapOfProfessions.put("Дизайнер",    new URL(staticURL + "дизайнеру"));
        mapOfProfessions.put("Кодер",       new URL(staticURL + "кодеру"));
        mapOfProfessions.put("Верстальщик", new URL(staticURL + "верстальщику"));
        mapOfProfessions.put("Фотограф",    new URL(staticURL + "фотографу"));
        mapOfProfessions.put("Копирайтер",  new URL(staticURL + "копирайтеру"));
        mapOfProfessions.put("Маркетолог",  new URL(staticURL + "маркетологу"));
        mapOfProfessions.put("Сеошник",     new URL(staticURL + "сеошнику"));
        mapOfProfessions.put("Водитель",    new URL(staticURL + "водителю"));
        mapOfProfessions.put("Музыкант",    new URL(staticURL + "музыканту"));
        mapOfProfessions.put("Фокусник",    new URL(staticURL + "фокуснику"));
        mapOfProfessions.put("Врач",        new URL(staticURL + "врачу"));
        mapOfProfessions.put("Студент",     new URL(staticURL + "студенту"));
        mapOfProfessions.put("Человек",     new URL(staticURL + "за жизнь"));
        mapOfProfessions.put("Мужчина",     new URL(staticURL + "для него"));
        mapOfProfessions.put("Женщина",     new URL(staticURL + "для неё"));
        } catch (MalformedURLException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
    public TreeMap<String, URL> getMap() {
        return mapOfProfessions;
    }

    static URL getURLbyKey(String key) {
        return mapOfProfessions.get(key);
    }
    void printAllProf() throws InterruptedException {
        int i = 0;
        System.out.println("---------------------------------");
        for (Map.Entry entry : mapOfProfessions.entrySet()) {
            if (i == 5 || i == 10 || i == 15) {
                System.out.println();
                Thread.sleep(500);
            }
            System.out.print(entry.getKey() + " ");
            i++;
        }

        System.out.println("\n---------------------------------");
    }
}

