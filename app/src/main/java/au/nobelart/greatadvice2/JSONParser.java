package au.nobelart.greatadvice2;

import android.util.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * artes on 3/10/16 tingaev@gmail.com
 **/
 class JSONParser {
    private JsonReader jsonReader;

     JSONParser(String professions) throws IOException {

        jsonReader = new JsonReader(
                new BufferedReader(
                        new InputStreamReader(
                                Professions.getURLbyKey(professions).openStream())));

    }
     Advices getAdvice() throws IOException {
        int id;
        String text;
        String sound;
        String name;

        jsonReader.beginObject();
        name = jsonReader.nextName();
        System.out.println(name);

        id = jsonReader.nextInt();
        jsonReader.nextName();
        System.out.println(id);

        text = jsonReader.nextString();
        text = text.replace("\u00a0", "").replace("&nbsp;", " ");
        System.out.println(text);

        jsonReader.nextName();
        sound = jsonReader.nextString();
        jsonReader.endObject();

        return new Advices(id, text);
    }
}
